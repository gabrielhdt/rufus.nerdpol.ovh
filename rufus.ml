open Tyxml.Html

let wan_addr = "https://rufus.nerdpol.ovh"

let rufus_title = title (txt "Rufus")

let nextcloud_par =
  div
    [ h2 ([txt "Nextcloud"])
    ; p [txt "The nextcloud instance is available";
        a ~a:[a_href (wan_addr ^ "/nextcloud")] [txt "here"]] ]

let streams =
  ul
    [ li [a ~a:[a_href "http://rufus:8000"] [txt "Flac 2.0 stream"]]
    ; li [a ~a:[a_href "http://rufus:8001"] [txt "Opus stream"]] ]

let radios =
  div
    [ h2 [txt "Radios"]
    ; p [txt "On the local network, you can listen to streams to"]; streams ]

let gaba =
  div
    [ h2 [txt "GABA"]
    ; p [ txt "A lovely php project is hosted"
        ; a ~a:[a_href (wan_addr ^ "/gaba")] [txt "here."]
        ; txt "The OWL for Open Wildlife Laboratory is a platform to register
animals" ] ]

let mainpage =
  html
    (head rufus_title [])
    (body [h1 [txt "Rufus"]; nextcloud_par; radios; gaba])

let _ =
  let file = open_out "index.html" in
  let fmt = Format.formatter_of_out_channel file in
  Format.fprintf fmt "%a@." (pp ~indent:true ()) mainpage;
  close_out file
