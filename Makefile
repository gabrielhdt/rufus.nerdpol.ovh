site_gen := rufus
all:
	ocamlfind ocamlopt rufus.ml -package tyxml -short-paths -linkpkg -o $(site_gen)
	./$(site_gen)

clean:
	rm -f *.cmo *.cmt *.cmi $(site_gen) index.html
